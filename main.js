/*
 * Created by Nikolay Babanov
 *
 * Tic Tac is a tic-tac-toe facebook chat bot
 *
 * */
'use strict';

require('./app/herokuServerFix');

const fs = require('fs');
const login = require("facebook-chat-api");
const acceptRequests = require('./app/acceptRequests');
const JokeService = require('./app/services/JokeService');

if (!process.env.EVOIRMENT) {
    var credentials = require('./app/config/cred');
} else {
    var credentials = require('./app/config/cred_heroku');
}


login({email: credentials.facebook.email, password: credentials.facebook.password}, function callback(err, api) {
    if (err) return console.error(err);

    api.setOptions({listenEvents: true});

    module.exports = {
        wrongMove: function (threadID) {
            api.sendMessage('Невалиден ход!', threadID);
        },
        announceWinner: function (winner, threadID) {
            api.sendMessage('Победителят е: ' + winner.name, threadID);
        },
        announceTurn: function (player, threadID) {
            api.sendMessage('На ход е: ' + player.name, threadID);
        },
        draw: function (data, threadID) {
            api.sendMessage({
                attachment: data
            }, threadID);
        },
        announceDraw: function (threadID) {
            api.sendMessage('Играта завърши на равно.', threadID);
        },
        announceNotInGame: function (threadID) {
            api.sendMessage('Играта не е пусната.\n\nЗа да пуснете нова игра или продължите стара първо напишете "/play".', threadID);
        },
        announceStats: function (playersArray, threadID) {
            api.sendMessage(playersArray[0].name + ': ' + playersArray[0].wins + ' победи\n' +
                playersArray[1].name + ': ' + playersArray[1].wins + ' победи', threadID);
        }
    };

    const gameController = require('./app/controllers/GameController');


    var stopListening = api.listen(function (err, event) {
        if (err) return console.error(err);

        console.log(event);

        switch (event.type) {
            case "message":

                if (event.body == '/play') {

                    var playersIDArray = event.participantIDs.filter(function (id) {
                        return parseInt(id) != parseInt(api.getCurrentUserID());
                    });

                    var playersNamesArray = event.participantNames.filter(function (name) {
                        return name != 'Tic';
                    });

                    var playersArray = [];

                    for (let i = 0; i < playersIDArray.length; i++) {
                        playersArray.push({
                            id: playersIDArray[i],
                            name: playersNamesArray[i]
                        });
                    }

                    gameController.init(parseInt(event.threadID), playersArray);

                } else if (event.body.search('/mark') != -1 || event.body.search('/Mark') != -1) {

                    var cellIdString = event.body.replace('/mark ', '').replace('/Mark ', '');

                    gameController.mark(parseInt(event.threadID), parseInt(event.senderID), cellIdString);

                } else if (event.body == '/stats') {
                    gameController.getStats(event.threadID);

                } else if (event.body == '/joke') {
                    api.markAsRead(event.threadID, function (err) {
                        if (err) console.log(err);
                    });

                        api.sendTypingIndicator(event.threadID, function () {
                            JokeService.fetchJoke().then(function (joke) {
                                console.log('JOKE:\n' + joke);
                                api.sendMessage(joke, event.threadID);
                            });
                        });
                }

                break;
            case "event":
                console.log(event);
                break;
        }
    });
});
