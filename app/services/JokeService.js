const Promise = require("bluebird");

var phantom = require('x-ray-phantom');
var Xray = require('x-ray');

var x = Xray();

const jokesURL = 'http://vic-bg.com/random';

function fetchJoke() {

    var fetch = function (resolve, reject) {
        x(jokesURL, '.joke-text')(function (err, joke) {
            if (err) {
                console.error(err);
                reject(err);
            } else {
                if (joke == '\n\n' || joke == '' || joke == ' ') {
                    fetch(resolve, reject);
                } else {
                    resolve(joke);
                }
            }
        });
    };

    return new Promise(function (resolve, reject) {
        fetch(resolve, reject);
    });
}

module.exports = {
    fetchJoke: fetchJoke
};


