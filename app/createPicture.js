const fs = require('fs');
const Promise = require("bluebird");

function createPicture () {
    return new Promise(function (resolve, reject) {
        var Canvas = require('canvas')
            , Image = Canvas.Image
            , canvas = new Canvas(200, 200)
            , ctx = canvas.getContext('2d');

        ctx.font = '30px Impact';
        ctx.rotate(.1);
        ctx.fillText("Awesome!", 50, 100);

        var te = ctx.measureText('Awesome!');
        ctx.strokeStyle = 'rgba(0,0,0,0.5)';
        ctx.beginPath();
        ctx.lineTo(50, 102);
        ctx.lineTo(50 + te.width, 102);
        ctx.stroke();

        var img = canvas.toDataURL();
        var data = img.replace(/^data:image\/\w+;base64,/, "");
        var buf = new Buffer(data, 'base64');

        fs.writeFile('test.jpg', buf, function () {
            resolve();
        });
    });
}

module.exports = createPicture;