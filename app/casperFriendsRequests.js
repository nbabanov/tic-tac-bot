var process = require('system');

if (!process.env.EVOIRMENT) {
    var credentials = require('./app/config/cred');
} else {
    var credentials = require('/app/app/config/cred_heroku');
}

var casper = require('casper').create({
    pageSettings: {
        loadImages: false,//The script is much faster when this field is set to false
        loadPlugins: false,
        userAgent: 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36'
    }
});

//First step is to open Facebook
casper.start().thenOpen("https://facebook.com", function () {});

//Now we have to populate username and password, and submit the form
casper.then(function () {
    this.evaluate(function (email, password) {
        document.getElementById("email").value = email;
        document.getElementById("pass").value = password;
        document.getElementById("u_0_w").click();
    }, {
        email: credentials.email,
        password: credentials.password
    });
});


casper.thenOpen('https://www.facebook.com/friends/requests/', function () {});

casper.then(function () {
    console.log('\n\n\nChecking for friends requests\n\n\n');

    casper.on('remote.message', function(msg) {
        this.echo(msg);
    });

    //Check if there are buttons for accepting friend requests and click them
    this.evaluate(function () {
        var requestsContainer = document.getElementsByClassName('phl')[0];
        var requestsArray = requestsContainer.getElementsByClassName('_42ft _4jy0 _4jy3 _4jy1');

        console.log('\nFriends requests count:' + requestsArray.length);

        for (var i = 0; i < requestsArray.length; i++) {
            requestsArray[i].click();
        }

        console.log('\nFriends requests check finished.');
    });
});

casper.run();