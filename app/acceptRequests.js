//Module for accepting friends requests

const credentials = require('/app/app/config/cred_heroku').facebook;

var execFile = require('child_process').execFile;
var spawn = require('child_process').spawn;

var acceptRequests = function () {
    if (!process.env.EVOIRMENT) {
        execFile('casperjs/bin/casperjs.exe', ['app/casperFriendsRequests.js'], function (err, data) {
            if (err) {
                console.error(err);
            }

            console.log(data.toString());
        });
    } else {
        var casperjs = spawn('casperjs', ['/app/app/casperFriendsRequests.js']);

        casperjs.on('error', function (err) {
            console.error(err);
        });

        casperjs.stdout.on('data', function (data) {
            var receivedData = data.toString();

            console.log(receivedData);

            if (receivedData == '\nFriends requests check finished.') {
                casperjs.kill();
            }
        });

    }

};

setInterval(function () {
    acceptRequests();
}, 1000);

module.exports = acceptRequests;