'use strict';

const Cell = require('./Cell');
const Player = require('./Player');

class Game {

    constructor(id, playersArray) {
        this.id = id;
        this.board = [];
        this.players = [];
        this.playersTurnIndex = null;

        this.initBoard();
        this.initPlayers(playersArray);
    }

    initBoard() {
        for (let i = 0; i < 3; i++) {
            this.board[i] = [];

            for (let j = 0; j < 3; j++) {
                this.board[i][j] = new Cell();
            }
        }
    }

    loadGame(storedGame) {

        if (storedGame.board) {
            for (let i = 0; i < this.board.length; i++) {
                for (let j = 0; j < this.board[i].length; j++) {
                    this.board[i][j].markedBy = storedGame.board[i][j].markedBy;
                    this.board[i][j].markSign = storedGame.board[i][j].markSign;
                }
            }
        }

        if (storedGame.players) {
            for (let i = 0; i < this.players.length; i++) {
                this.players[i].wins = storedGame.players[i].wins;
            }
        }

        if (storedGame.playersTurnIndex) {
            this.playersTurnIndex = storedGame.playersTurnIndex;
        }

    }

    initPlayers(playersArray) {
        for (let i = 0; i < playersArray.length; i++) {
            this.players[i] = new Player({
                name: playersArray[i].name,
                id: playersArray[i].id,
                gameController: this
            });
        }

        this.playersTurnIndex = 0;

        console.log('=================');
        console.log(this.players);
        console.log('=================');
    }

    markCell(cellIdString, playerId) {
        if (this.board[cellIdString[0]][cellIdString[1]].markedBy != null) {
            return false;

        } else {
            this.board[cellIdString[0]][cellIdString[1]].markBy(playerId, this.playersTurnIndex);

            console.log('=================');
            console.log(this.board);
            console.log('=================');

            this.changeTurn();

            return true;
        }
    }

    changeTurn() {
        this.playersTurnIndex = this.players.length - 1 - this.playersTurnIndex;
    }

    get playerToMark() {
        return this.players[this.playersTurnIndex];
    }

    get winner() {
        var testArray = [];
        var winner = null;

        testArray[0] = this.checkRow();
        testArray[1] = this.checkCol();
        testArray[2] = this.checkDiagonal();
        testArray[3] = this.checkAntiDiagonal();

        var winnerID = testArray.filter(function (obj) {
            return obj != null;
        });

        if (winnerID.length > 0) {
            winnerID = winnerID[0];

            for (let i = 0; i < this.players.length; i++) {
                if (this.players[i].id == winnerID) {
                    winner = this.players[i];
                }
            }
        }

        return winner;
    }

    checkRow() {
        var winner = null;


        for (let i = 0; i < this.board.length; i++) {
            let equalMarks = 0;

            for (let j = 0; j < this.board[i].length - 1; j++) {
                if (this.board[i][j].markedBy == this.board[i][j + 1].markedBy) {
                    equalMarks++;
                } else {
                    break;
                }
            }

            if (equalMarks == this.board.length - 1) {
                winner = this.board[i][0].markedBy;
                break;
            }
        }

        return winner;
    }

    checkCol() {
        var winner = null;


        for (let i = 0; i < this.board.length; i++) {
            let equalMarks = 0;

            for (var j = 0; j < this.board[i].length - 1; j++) {
                if (this.board[j][i].markedBy == this.board[j + 1][i].markedBy) {
                    equalMarks++;
                } else {
                    break;
                }
            }

            if (equalMarks == this.board.length - 1) {
                winner = this.board[j - 1][i].markedBy;
            }
        }

        return winner;
    }

    checkDiagonal() {
        var winner = null;

        var equalMarks = 0;

        for (var i = 0; i < this.board.length - 1; i++) {

            if (this.board[i][i].markedBy == this.board[i + 1][i + 1].markedBy) {
                equalMarks++;
            } else {
                break;
            }
        }

        if (equalMarks == this.board.length - 1) {
            winner = this.board[i][i].markedBy;
        }

        return winner;
    }

    checkAntiDiagonal() {
        var winner = null;

        var equalMarks = 0;

        for (var i = 0; i < this.board.length - 1; i++) {
            if (this.board[i][this.board.length - 1 - i].markedBy == this.board[i + 1][this.board.length - 2 - i].markedBy) {
                equalMarks++;
            } else {
                break;
            }

            if (equalMarks == this.board.length - 1) {
                winner = this.board[i][i].markedBy;
            }

        }

        return winner;
    }
}

module.exports = Game;