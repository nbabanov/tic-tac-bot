'use strict';

class Player {
    constructor(player) {
        this.name = player.name;
        this.id = player.id;
        this.wins = 0;
    }
}

module.exports = Player;