'use strict';

class Cell {

    constructor() {
        this.markedBy = null;
        this.markSign = null;
    }

    markBy(playerId, sign) {
        this.markedBy = playerId;
        this.markSign = sign;
    }
}

module.exports = Cell;