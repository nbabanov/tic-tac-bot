module.exports = {
    facebook: {
        email: process.env.fb_email,
        password: process.env.fb_pass
    },
    redis: {
        port: process.env.redis_port,          // Redis port
        host: process.env.redis_host,   // Redis host
        family: process.env.redis_family,           // 4 (IPv4) or 6 (IPv6)
        password: process.env.redis_pass,
        db: process.env.redis_db
    }
};