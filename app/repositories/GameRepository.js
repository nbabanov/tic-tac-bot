const Redis = require('ioredis');

if (!process.env.EVOIRMENT) {
    var cred = require('../config/cred').redis;
} else {
    var cred = require('../config/cred_heroku').redis;
}

const redisInstance = new Redis({
    port: cred.port,          // Redis port
    host: cred.host,   // Redis host
    family: cred.family,           // 4 (IPv4) or 6 (IPv6)
    password: cred.password,
    db: cred.db
});




module.exports = {
    setGame: function (currentGame) {
        redisInstance.set(currentGame.threadId, JSON.stringify(currentGame));
    },
    getGame: function (threadId) {
        return redisInstance.get(threadId);
    },
    setGameEnd: function (currentGame) {
        delete currentGame.gameInstance.board;
        delete currentGame.gameInstance.playersTurnIndex;
        delete currentGame.playerMoves;

        redisInstance.set(currentGame.threadId, JSON.stringify(currentGame));
    }
};