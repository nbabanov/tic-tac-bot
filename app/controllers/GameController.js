'use strict';

const Game = require('../models/Game');
const chatApi = require('../../main');
const fs = require('fs');
const GameRepository = require('../repositories/GameRepository');

const imageCoords = require('../config/constants').imageCoords;


var Canvas = require('canvas')
    , Image = Canvas.Image
    , canvas = new Canvas(218, 218)
    , ctx = canvas.getContext('2d');


var xSignResource = fs.readFileSync('./app/img/x.png');
var oSignResource = fs.readFileSync('./app/img/o.png');
var boardResource = fs.readFileSync('./app/img/board.png');


var gameController = {
    activeGames: {},

    /**
     * Initializes a game
     * @param {number} threadId
     * @param {array} playersArray
     */
    init: function (threadId, playersArray) {

        if (typeof this.activeGames[threadId] == 'undefined') {
            this.startGame(threadId, playersArray);
        } else {
            this.announceTurn(threadId);
        }


        console.log(this.activeGames);
    },

    /**
     * Creates a new game
     * @param {number} threadId
     * @param {array} playersArray
     */
    createNewGame: function (threadId, playersArray) {
        this.activeGames[threadId] = {
            gameInstance: new Game(threadId, playersArray),
            threadId: threadId,
            playerMoves: 0
        };
    },

    /**
     * Marks a cell with player's sign (O or X)
     *
     * The format of the cellIdString must be:
     * 'xy' or 'x y', x and y are numbers,
     * which signify which cell on the board must be
     * marked.
     *
     * @param {number} threadId
     * @param {number} playerId
     * @param {string} cellIdString
     * @returns {boolean}
     */
    mark: function (threadId, playerId, cellIdString) {

        if (!this.activeGames[threadId]) {
            chatApi.announceNotInGame(threadId);
            return false;
        }

        if (playerId != this.getPlayerToMove(threadId)) {
            this.announceTurn(threadId);
            return false;
        }


        cellIdString = cellIdString.toString().replace(' ', '');

        if (cellIdString.length != 2) {
            chatApi.wrongMove(threadId);

        } else if (
            parseInt(cellIdString[0]) - 1 < 0 || parseInt(cellIdString[0]) - 1 > 2
            || parseInt(cellIdString[1]) - 1 < 0 || parseInt(cellIdString[1]) - 1 > 2
            || isNaN(parseInt(cellIdString[1])) || isNaN(parseInt(cellIdString[0]))
        ) {

            chatApi.wrongMove(threadId);

        } else {
            var tempString = cellIdString.split('');
            tempString[0]--;
            tempString[1]--;

            tempString = tempString.join('');

            var success = this.activeGames[threadId].gameInstance.markCell(tempString, playerId);

            if (!success) {
                chatApi.wrongMove(threadId);
            } else {
                this.activeGames[threadId].playerMoves++;
                this.saveGame(threadId);
            }
        }

        //If we have a winner, the next turn is not announced (no next turn)
        if (this.activeGames[threadId].playerMoves >= 3 && this.checkWinner(threadId)) {
            return false;

        }

        this.announceTurn(threadId);
    },

    /**
     * Returns the id of the player who is on the move
     * @param {number} threadId
     * @returns {string} id
     */
    getPlayerToMove: function (threadId) {
        return this.activeGames[threadId].gameInstance.playerToMark.id;
    },

    /**
     * Announces who is on the move
     * @param {number} threadId
     * @returns {boolean}
     */
    announceTurn: function (threadId) {
        if (this.activeGames[threadId].playerMoves == 9) {
            chatApi.announceDraw(threadId);
            this.endGame(threadId);
            return false;
        }

        chatApi.announceTurn(this.activeGames[threadId].gameInstance.playerToMark, threadId);
        this.drawBoard(threadId);
    },

    /**
     * Checks the winner of the game and announces him/her
     * @param {number} threadId
     * @returns {boolean}
     */
    checkWinner: function (threadId) {
        var winner = this.activeGames[threadId].gameInstance.winner;
        if (winner) {
            this.activeGames[threadId].gameInstance.winner.wins++;
            chatApi.announceWinner(winner, threadId);
            this.endGame(threadId);
            return true;
        }

        return false;
    },

    /**
     * Draws the current state of the game board
     * @param {number} threadId
     */
    drawBoard: function (threadId) {

        var boardImage = new Image();
        boardImage.src = boardResource;
        ctx.drawImage(boardImage, 0, 0, boardImage.width, boardImage.height);


        if (this.activeGames[threadId].playerMoves != 0) {

            var xSign = new Image();
            xSign.src = xSignResource;
            var oSign = new Image();
            oSign.src = oSignResource;

            for (let i = 0; i < this.activeGames[threadId].gameInstance.board.length; i++) {
                for (let j = 0; j < this.activeGames[threadId].gameInstance.board.length; j++) {

                    if (this.activeGames[threadId].gameInstance.board[i][j].markSign != null) {

                        if (this.activeGames[threadId].gameInstance.board[i][j].markSign == 0) {
                            ctx.drawImage(oSign, imageCoords[i][j].x, imageCoords[i][j].y, oSign.width, oSign.height);
                        } else {
                            ctx.drawImage(xSign, imageCoords[i][j].x, imageCoords[i][j].y, xSign.width, xSign.height);
                        }
                    }
                }
            }


            var canvasData = canvas.toDataURL();
            var imageData = canvasData.replace(/^data:image\/\w+;base64,/, '');
            var imageBuffer = new Buffer(imageData, 'base64');


            var tempImageName = (new Date).getTime();

            fs.writeFile('./temp/' + tempImageName + '.jpg', imageBuffer, function () {
                chatApi.draw(fs.createReadStream('./temp/' + tempImageName + '.jpg'), threadId);
            });

        } else {
            chatApi.draw(fs.createReadStream('./app/img/board.png'), threadId);
        }

    },

    saveGame: function (threadId) {
        GameRepository.setGame(this.activeGames[threadId]);
    },

    endGame: function (threadId) {
        GameRepository.setGameEnd(this.activeGames[threadId]);
        delete this.activeGames[threadId];
    },

    startGame: function (threadId, playersArray) {
        var self = this;

        this.createNewGame(threadId, playersArray);

        this.loadGame(threadId, function (storedGame) {
            if (typeof storedGame != 'undefined' && storedGame != null) {
                self.activeGames[threadId].gameInstance.loadGame(storedGame.gameInstance);
            }
            self.announceTurn(threadId);
        });
    },

    loadGame: function (threadId, callback) {
        var self = this;

        GameRepository.getGame(threadId).then(function (storedGame) {
            if (typeof storedGame != 'undefined' && storedGame != null) {

                storedGame = JSON.parse(storedGame);

                console.log('STORED GAME:');
                console.log(storedGame);

                if (storedGame.playerMoves) {
                    self.activeGames[threadId].playerMoves = storedGame.playerMoves;
                }

            }
            callback(storedGame);
        });
    },

    getStats: function (threadId) {
        var self = this;

        if (this.activeGames[threadId]) {
            chatApi.announceStats(this.activeGames[threadId].gameInstance.players, threadId);
        } else {
            this.loadGame(threadId, function (storedGame) {
                chatApi.announceStats(storedGame.gameInstance.players, threadId);
            });
        }
    }
};


module.exports = gameController;
